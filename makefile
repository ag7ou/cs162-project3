# Lab 4 - Rob Johnson - PCC CS162 - 10/12/2018
CC=g++
CFLAGS= -c -Wall
all: prog
prog: songmain.o song.o
	$(CC) songmain.o song.o -o prog
songmain.o: songmain.cpp
	$(CC) $(CFLAGS) songmain.cpp
song.o: song.cpp
	$(CC) $(CFLAGS) song.cpp
clean:
	rm -rf *.o
