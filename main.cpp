// Project 3 - Rob Johnson - pcc cs162 - 10/27/18

#include <iostream>
#include <fstream>
#include "main.h"
#include "task.h"
#include "string.h"

using namespace std;


// date comparison function
// inputs: year, month, day, task
// returns -1 if the date in the task is earlier than the supplied date
// returns 0 if the date in the task is the same as the supplied date
// returns 1 if the date in the task is late than the supplied date
int date_compare(int year, int month, int day, int taskyear, int taskmonth, int taskday)
{
    int result;

    if(taskyear < year)
    {
        result = -1;
    }
    else if(taskyear > year)
    {
        result = 1;
    }
    else // taskyear == year
    {
        if(taskmonth < month)
        {
            result = -1;
        }
        else if(taskmonth > month)
        {
            result = 1;
        }
        else // taskmonth == month
        {
            if(taskday < day)
            {
                result = -1;
            }
            else if(taskday > day)
            {
                result = 1;
            }
            else // taskday == day
            {
                result = 0;
            }
        }
    }

    return result;
}


// tests to determine whether a task falls within a provided date range
bool date_within_range(int year0, int month0, int day0, int year1, int month1, int day1, Task task)
{
    // by adding the result of date_compare for each of the dates and taking the absolute value of the result
    // it is possible to determine whether the date is within the range, regardless of the order in which the date range is provided
    // if result == 0 then the task date is within the range (or both dates are the same and task date is also the same)
    // if result == 1 then the task date falls on one of the dates
    // if result == 2 then the task is earlier or later than both of the dates and is not valid
    bool result;
    int sum = date_compare(year0, month0, day0, task.GetYear(), task.GetMonth(), task.GetDay())
            + date_compare(year1, month1, day1, task.GetYear(), task.GetMonth(), task.GetDay());

    if(sum > -2 && sum < 2)
    {
        result = true;
    }
    else
    {
        result = false;
    }

    return result;
}


// print all tasks
void print_all(Task task[MAX_TASKS], int task_count)
{
    for(int i = 0; i < task_count; ++i)
    {
        task[i].print();
    }
}


void input_number(int& number, int upper_limit, int lower_limit)
{
    number = 0;

    while(number == 0)
    {
        cin >> number;
        while(cin.fail() || number < lower_limit || number > upper_limit)
        {
            cin.clear();
            cin.ignore(100, '\n');
            cout << "Invalid entry. Should be an integer between " << lower_limit << " and " << upper_limit << endl;
            cin >> number;
        }
    }
}


void input_date(int& year, int& month, int& day)
{
    cout << "Enter year: ";
    input_number(year, YEAR_UPPER_LIMIT, YEAR_LOWER_LIMIT);

    cout << "Enter month: ";
    input_number(month, MONTH_UPPER_LIMIT, MONTH_LOWER_LIMIT);

    cout << "Enter day: ";
    input_number(day, DAY_UPPER_LIMIT, DAY_LOWER_LIMIT);
}


void print_range(Task task[MAX_TASKS], int task_count)
{
    int year0, year1, month0, month1, day0, day1;
    cout << "Enter the first date." << endl;
    input_date(year0, month0, day0);
    cout << "Enter the second date." << endl;
    input_date(year1, month1, day1);

    for(int i = 0; i < task_count; ++i)
    {
        if(date_within_range(year0, month0, day0, year1, month1, day1, task[i]))
        {
            task[i].print();
        }
    }
}


void add_task(Task *task, int& len)
{
    char name[MAX_STRING], desc[MAX_STRING];
    int year, month, day;

    cin.ignore(MAX_STRING,'\n');

    cout << "Enter task name: ";
    cin.getline(name, MAX_STRING);
    task[len].SetName(name);

    cout << "Enter task description: ";
    cin.getline(desc, MAX_STRING);
    task[len].SetDescription(desc);

    input_date(year, month, day);
    task[len].SetYear(year);
    task[len].SetMonth(month);
    task[len].SetDay(day);

    task[len].SetComplete(false);  // Always setting to false b/c it wouldn't make sense to add a completed item to the list.

    ++len;
}


void mark_task_complete(Task *task, int len)
{
    bool notMatched = true;
    char searchName[MAX_STRING];

    cin.ignore(MAX_STRING,'\n');
    cout << "Enter the name of the task to mark complete." << endl;
    cin.getline(searchName, MAX_STRING);

    for(int i = 0; i < len; ++i)
    {
        if(strcmp(task[i].GetName(), searchName) == 0)
        {
            notMatched = false;
            task[i].SetComplete(true);
            cout << "Task set as completed." << endl;
            break;
        }
    }

    if(notMatched)
    {
        cout << "Unable to locate a task with that name." << endl;
    }
}


// returns the number of entries that have been read
int fileRead(Task *task)
{
    int len = 0;
    ifstream infile;

    infile.open(INPUT_FILENAME);
    if(infile)
    {
        infile.peek(); // tests for end of file w/o moving the read pointer ahead
        for(int i = 0; !infile.eof(); ++i)
        {
            char name[MAX_STRING];
            infile.getline(name, MAX_STRING);
            task[i].SetName(name);

            char description[MAX_STRING];
            infile.getline(description, MAX_STRING);
            task[i].SetDescription(description);

            int year, month, day;
            infile >> month;
            infile.ignore(MAX_STRING,'\n');

            infile >> day;
            infile.ignore(MAX_STRING,'\n');

            infile >> year;
            infile.ignore(MAX_STRING,'\n');

            task[i].SetYear(year);
            task[i].SetMonth(month);
            task[i].SetDay(day);

            bool complete;
            infile >> complete;
            infile.ignore(MAX_STRING,'\n');
            task[i].SetComplete(complete);

            ++len;
            if(len >= MAX_TASKS)    // prevent pverflow if there are more tasks in the file than the array
            {
                break;
            }

            infile.peek();
        }
        infile.close();
    }
    else
    {
        cout << "Error opening input file." << endl;
    }

    return len;
}


void fileWrite(Task *task, int len)
{
    ofstream outfile;

    outfile.open(OUTPUT_FILENAME);

    for(int i = 0; i < len; ++i)
    {
        outfile << task[i].GetName() << endl;
        outfile << task[i].GetDescription() << endl;
        outfile << task[i].GetMonth() << endl;
        outfile << task[i].GetDay() << endl;
        outfile << task[i].GetYear() << endl;
        outfile << task[i].GetComplete() << endl;
    }

    outfile.close();
}


void menu(Task *task, int task_count)
{
    bool repeat = true;

    do
    {
        char response;

        cout << endl << "********MAIN MENU**********\n1 - Print All Tasks\n2 - Print Tasks in Date Range\n3 - Add Task\n4 - Complete Task\n5 - quit\nEnter choice: ";
        cin >> response;

        switch(response)
        {
            case '1':
                print_all(task, task_count);
                break;
            case '2':
                print_range(task, task_count);
                break;
            case '3':
                add_task(task, task_count);
                break;
            case '4':
                mark_task_complete(task, task_count);
                break;
            case '5':
                repeat = false;
                break;
            default:
                cout << "Invalid entry. Please try again with a valid input." << endl;
                if(cin.fail())
                {
                    cin.clear();
                    cin.ignore(MAX_STRING, '\n');
                }
                break;
        }
    } while(repeat == true);
}


int main(void)
{
    int task_count = 0;
    struct Task task[MAX_TASKS];

    cout << "******* Task Manager App *******" << endl;

    task_count = fileRead(task);    // Load tasks from file
    menu(task, task_count);
    fileWrite(task, task_count);    // save tasks to file.

    return 0;
}
